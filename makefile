client: client.c xdg-shell-protocol.c xdg-shell-client-protocol.h
	cc -o client client.c xdg-shell-protocol.c -lwayland-client -lrt

xdg-shell-protocol.c:
	wayland-scanner private-code \
		< /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml \
		> xdg-shell-protocol.c

xdg-shell-client-protocol.h:
	wayland-scanner client-header \
		< /usr/share/wayland-protocols/stable/xdg-shell/xdg-shell.xml \
		> xdg-shell-client-protocol.h

clean:
	rm -f client xdg-shell-protocol.c xdg-shell-client-protocol.h
